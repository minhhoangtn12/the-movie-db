import 'package:flavoring/model/entity/user/user_entity.dart';
import 'package:flavoring/model/request/auth/login_request.dart';
import 'package:flavoring/model/request/auth/register_request.dart';

import 'package:flavoring/domain/repository/auth_repository.dart';

class AuthRepositoryImpl implements AuthRepository {
  AuthRepositoryImpl();

  @override
  Future<void> registerAccount(RegisterRequest param) async {}

  @override
  Future<UserEntity> login(LoginRequest param) async {
    return const UserEntity(
        id: '123', email: '123', password: '123', fullName: '3123123');
  }

  @override
  Future<void> logout() async {}

  @override
  UserEntity autoLogin(String token) {
    return const UserEntity(
        id: '123', email: '123', password: '123', fullName: '3123123');
  }
}
