import 'package:dio/dio.dart';
import 'package:flavoring/data/data_source/remote/collection_service.dart';
import 'package:flavoring/domain/repository/collection_repository.dart';
import 'package:flavoring/model/entity/collection/collection_detail.dart';
import 'package:flavoring/model/exception/api_exception.dart';

class CollectionRepositoryImpl implements CollectionRepository {
  final CollectionService service;

  const CollectionRepositoryImpl(
    this.service,
  );

  @override
  Future<CollectionDetail> getCollectionDetail(int id) async {
    try {
      return await service.getDetail(id);
    } on DioError catch (e) {
      throw (ApiException(e));
    }
  }
}
