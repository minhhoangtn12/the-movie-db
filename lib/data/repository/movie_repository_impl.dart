import 'package:dio/dio.dart';
import 'package:flavoring/data/data_source/remote/movie_service.dart';
import 'package:flavoring/domain/repository/movie_repository.dart';
import 'package:flavoring/model/entity/movie/movie_detail.dart';
import 'package:flavoring/model/entity/movie/movie_list_item.dart';
import 'package:flavoring/model/entity/movie/movie_review.dart';
import 'package:flavoring/model/exception/api_exception.dart';
import 'package:flavoring/model/response/array_response.dart';
import 'package:flavoring/model/response/credit_response.dart';

class MovieRepositoryImpl extends MovieRepository {
  final MovieService movieService;

  MovieRepositoryImpl(this.movieService);

  @override
  Future<ArrayResponse<MovieListItem>> getPopularMovie(int page) async {
    try {
      return await movieService.getPopular(page);
    } on DioError catch (e) {
      throw (ApiException(e));
    }
  }

  @override
  Future<MovieDetail> getDetailMovie(int id) async {
    try {
      return await movieService.getDetail(id);
    } on DioError catch (e) {
      throw (ApiException(e));
    }
  }

  @override
  Future<MovieDetail> getLatestMovie() async {
    try {
      return await movieService.getLatest();
    } on DioError catch (e) {
      throw (ApiException(e));
    }
  }

  @override
  Future<ArrayResponse<MovieListItem>> getRecommendationMovie(
      int id, int page) async {
    try {
      return await movieService.getRecommendation(id, page);
    } on DioError catch (e) {
      throw (ApiException(e));
    }
  }

  @override
  Future<ArrayResponse<MovieReview>> getReviewMovie(int id, int page) async {
    try {
      return await movieService.getReview(id, page);
    } on DioError catch (e) {
      throw (ApiException(e));
    }
  }

  @override
  Future<ArrayResponse<MovieListItem>> getSimilarMovie(int id, int page) async {
    try {
      return await movieService.getSimilar(id, page);
    } on DioError catch (e) {
      throw (ApiException(e));
    }
  }

  @override
  Future<ArrayResponse<MovieListItem>> getTopRatedMovie(int page) async {
    try {
      return await movieService.getTopRated(page);
    } on DioError catch (e) {
      throw (ApiException(e));
    }
  }

  @override
  Future<ArrayResponse<MovieListItem>> getUpcomingMovie(int page) async {
    try {
      return await movieService.getUpcoming(page);
    } on DioError catch (e) {
      throw (ApiException(e));
    }
  }

  @override
  Future<CreditResponse> getCreditMovie(int id) async {
    try {
      return await movieService.getCredit(id);
    } on DioError catch (e) {
      throw (ApiException(e));
    }
  }
}
