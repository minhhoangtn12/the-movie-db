import 'package:dio/dio.dart';
import 'package:flavoring/model/entity/movie/movie_detail.dart';
import 'package:flavoring/model/entity/movie/movie_list_item.dart';
import 'package:flavoring/model/entity/movie/movie_review.dart';
import 'package:flavoring/model/response/array_response.dart';
import 'package:flavoring/model/response/credit_response.dart';
import 'package:retrofit/http.dart';

part 'movie_service.g.dart';

@RestApi()
abstract class MovieService {
  factory MovieService(Dio dio, {String baseUrl}) = _MovieService;

  @GET('/popular')
  Future<ArrayResponse<MovieListItem>> getPopular(@Query('page') int page);

  @GET('/top_rated')
  Future<ArrayResponse<MovieListItem>> getTopRated(@Query('page') int page);

  @GET('/upcoming')
  Future<ArrayResponse<MovieListItem>> getUpcoming(@Query('page') int page);

  @GET('/latest')
  Future<MovieDetail> getLatest();

  @GET('/{movie_id}')
  Future<MovieDetail> getDetail(@Path('movie_id') int id);

  @GET('/{movie_id}/similar')
  Future<ArrayResponse<MovieListItem>> getSimilar(
      @Path('movie_id') int id, @Query('page') int page);

  @GET('/{movie_id}/recommendations')
  Future<ArrayResponse<MovieListItem>> getRecommendation(
      @Path('movie_id') int id, @Query('page') int page);

  @GET('/{movie_id}/reviews')
  Future<ArrayResponse<MovieReview>> getReview(
      @Path('movie_id') int id, @Query('page') int page);

  @GET('/{movie_id}/credits')
  Future<CreditResponse> getCredit(@Path('movie_id') int id);
}
