import 'package:dio/dio.dart';
import 'package:flavoring/model/entity/collection/collection_detail.dart';
import 'package:retrofit/http.dart';

part 'collection_service.g.dart';

@RestApi()
abstract class CollectionService {
  factory CollectionService(Dio dio, {String baseUrl}) = _CollectionService;

  @GET('/{collection_id}')
  Future<CollectionDetail> getDetail(@Path('collection_id') int id);
}
