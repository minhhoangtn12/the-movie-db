import 'package:dio/dio.dart';

class AuthInterceptor extends InterceptorsWrapper {
  final String apiKey;

  AuthInterceptor({required this.apiKey});

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    options.queryParameters['api_key'] = apiKey;
    options.queryParameters['language'] = 'en';
    super.onRequest(options, handler);
  }
}
