

import 'package:flavoring/model/entity/user/user_entity.dart';

abstract class UserDAO {
  List<UserEntity> getAllUser();
  UserEntity getUser(String userId);
  Future<void> addUser(UserEntity user);
}
