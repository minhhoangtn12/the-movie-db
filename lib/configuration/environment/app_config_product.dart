import 'package:flavoring/configuration/environment/env.dart';

class AppConfigProduct extends AppConfig {
  @override
  String get appName => 'Flavoring Prod';

  @override
  String get imageUrl => 'http://image.tmdb.org/t/p/';

  @override
  String get baseUrl => 'https://api.themoviedb.org/3';

  @override
  String get apiKey => '9de0d58a8402430f4b2b84c9d82568ed';

  @override
  AppEnv get flavor => AppEnv.prod;
}
