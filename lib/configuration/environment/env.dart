export 'app_config_dev.dart';
export 'app_config_product.dart';

enum AppEnv { dev, prod }

abstract class AppConfig {
  String get baseUrl;

  String get imageUrl;

  String get apiKey;

  AppEnv get flavor;

  String get appName;
}
