class AppImage {
  static const String _basePath = 'assets/images';

  static const String icLogo = '$_basePath/ic_logo.jpg';
}
