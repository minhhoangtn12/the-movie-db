import 'package:flavoring/model/entity/actor/actor_list_item.dart';
import 'package:json_annotation/json_annotation.dart';

part 'credit_response.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class CreditResponse {
  final int? id;
  final List<ActorListItem>? cast;
  final List<ActorListItem>? crew;

  factory CreditResponse.fromJson(Map<String, dynamic> json) =>
      _$CreditResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CreditResponseToJson(this);

  const CreditResponse({
    this.id,
    this.cast,
    this.crew,
  });
}
