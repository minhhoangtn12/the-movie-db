import 'package:json_annotation/json_annotation.dart';

part 'array_response.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class ArrayResponse<T> {
  final int? page;

  @JsonKey(name: 'total_results')
  final int? totalResult;

  @JsonKey(name: 'total_pages')
  final int? totalPage;

  final List<T>? results;

  factory ArrayResponse.fromJson(
          Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$ArrayResponseFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) =>
      _$ArrayResponseToJson(this, toJsonT);

  const ArrayResponse({
    this.page,
    this.totalResult,
    this.totalPage,
    this.results,
  });
}
