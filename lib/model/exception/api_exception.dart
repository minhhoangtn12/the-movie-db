import 'package:dio/dio.dart';

class ApiException {
  final int errorCode;
  final String errorMessage;
  final DioError errorObject;

  factory ApiException(DioError apiError) {
    switch (apiError.type) {
      case DioErrorType.response:
        return ApiException._(
            errorObject: apiError,
            errorMessage: apiError.response!.statusMessage!,
            errorCode: apiError.response!.statusCode!);
      case DioErrorType.cancel:
        return ApiException._(
          errorObject: apiError,
          errorMessage: 'Request đã bị hủy',
          errorCode: 0,
        );
      case DioErrorType.receiveTimeout:
      case DioErrorType.connectTimeout:
      case DioErrorType.sendTimeout:
        return ApiException._(
            errorObject: apiError,
            errorMessage: 'TimeOut!',
            errorCode: apiError.response?.statusCode ?? 0);
      case DioErrorType.other:
        return ApiException._(
            errorMessage: 'Other Error', errorCode: 0, errorObject: apiError);
    }
  }

  ApiException._(
      {required this.errorMessage,
      required this.errorCode,
      required this.errorObject});
}
