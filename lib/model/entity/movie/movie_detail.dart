import 'package:flavoring/model/entity/collection/collection_detail.dart';
import 'package:flavoring/model/entity/movie/movie_genre.dart';
import 'package:flavoring/model/entity/movie/movie_product_company.dart';
import 'package:flavoring/model/entity/movie/movie_product_country.dart';
import 'package:json_annotation/json_annotation.dart';

part 'movie_detail.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class MovieDetail {
  final bool? adult;

  final String? backdropPath;

  @JsonKey(name: 'belongs_to_collection')
  final CollectionDetail? collection;

  final int? budget;

  final List<MovieGenre>? genres;

  final String? homepage;

  final int? id;

  final String? imdbId;

  final String? originalLanguage;

  final String? originalTitle;

  final String? overview;

  final double? popularity;

  final String? posterPath;

  final List<MovieProductCompany>? productionCompanies;

  final List<MovieProductCountry>? productionCountries;

  final String? releaseDate;

  final int? revenue;

  final int? runtime;

  final String? status;

  final String? tagline;

  final String? title;

  final bool? video;

  final double? voteAverage;

  final int? voteCount;

  double get starVoteRate =>
      voteAverage == null ? 0 : ((voteAverage!).round() / 2);

  factory MovieDetail.fromJson(Map<String, dynamic> json) =>
      _$MovieDetailFromJson(json);

  Map<String, dynamic> toJson() => _$MovieDetailToJson(this);

  const MovieDetail({
    this.adult,
    this.backdropPath,
    this.collection,
    this.budget,
    this.genres,
    this.homepage,
    this.id,
    this.imdbId,
    this.originalLanguage,
    this.originalTitle,
    this.overview,
    this.popularity,
    this.posterPath,
    this.productionCompanies,
    this.productionCountries,
    this.releaseDate,
    this.revenue,
    this.runtime,
    this.status,
    this.tagline,
    this.title,
    this.video,
    this.voteAverage,
    this.voteCount,
  });
}
