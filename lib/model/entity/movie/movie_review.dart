import 'package:json_annotation/json_annotation.dart';

part 'movie_review.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class MovieReview {
  final String? author;
  final MovieReviewAuthor? authorDetails;
  final String? content;
  final String? createdAt;
  final String? id;
  final String? updatedAt;
  final String? url;

  factory MovieReview.fromJson(Map<String, dynamic> json) =>
      _$MovieReviewFromJson(json);

  Map<String, dynamic> toJson() => _$MovieReviewToJson(this);

  const MovieReview({
    this.author,
    this.authorDetails,
    this.content,
    this.createdAt,
    this.id,
    this.updatedAt,
    this.url,
  });
}

@JsonSerializable(fieldRename: FieldRename.snake)
class MovieReviewAuthor {
  final String? name;
  final String? username;
  final String? avatarPath;
  final double? rating;

  factory MovieReviewAuthor.fromJson(Map<String, dynamic> json) =>
      _$MovieReviewAuthorFromJson(json);

  Map<String, dynamic> toJson() => _$MovieReviewAuthorToJson(this);

  const MovieReviewAuthor({
    this.name,
    this.username,
    this.avatarPath,
    this.rating,
  });
}
