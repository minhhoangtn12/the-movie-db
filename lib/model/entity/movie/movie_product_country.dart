import 'package:json_annotation/json_annotation.dart';

part 'movie_product_country.g.dart';

@JsonSerializable()
class MovieProductCountry {
  @JsonKey(name: 'iso_3166_1')
  final String? code;

  final String? name;

  factory MovieProductCountry.fromJson(Map<String, dynamic> json) =>
      _$MovieProductCountryFromJson(json);

  Map<String, dynamic> toJson() => _$MovieProductCountryToJson(this);

  const MovieProductCountry({
    this.code,
    this.name,
  });
}
