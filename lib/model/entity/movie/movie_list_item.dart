import 'package:json_annotation/json_annotation.dart';

part 'movie_list_item.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class MovieListItem {
  final String? posterPath;
  final bool? adult;
  final String? overview;

  final String? releaseDate;
  final List<int>? genreIds;
  final int? id;

  final String? originalTitle;

  final String? originalLanguage;
  final String? title;

  final String? backdropPath;
  final double? popularity;

  final int? voteCount;

  final double? voteAverage;

  String get ratePercentText =>
      voteAverage == null ? 'NR' : '${(voteAverage! * 10).toInt()}';

  factory MovieListItem.fromJson(Map<String, dynamic> json) =>
      _$MovieListItemFromJson(json);

  Map<String, dynamic> toJson() => _$MovieListItemToJson(this);

  const MovieListItem({
    this.posterPath,
    this.adult,
    this.overview,
    this.releaseDate,
    this.genreIds,
    this.id,
    this.originalTitle,
    this.originalLanguage,
    this.title,
    this.backdropPath,
    this.popularity,
    this.voteCount,
    this.voteAverage,
  });
}
