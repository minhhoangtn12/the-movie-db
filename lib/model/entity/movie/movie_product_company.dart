import 'package:json_annotation/json_annotation.dart';

part 'movie_product_company.g.dart';

@JsonSerializable()
class MovieProductCompany {
  final int? id;

  @JsonKey(name: 'logo_path')
  final String? logoPath;

  final String? name;

  @JsonKey(name: 'origin_country')
  final String? originCountry;

  factory MovieProductCompany.fromJson(Map<String, dynamic> json) =>
      _$MovieProductCompanyFromJson(json);

  Map<String, dynamic> toJson() => _$MovieProductCompanyToJson(this);

  const MovieProductCompany({
    this.id,
    this.logoPath,
    this.name,
    this.originCountry,
  });
}
