import 'package:json_annotation/json_annotation.dart';

part 'actor_list_item.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class ActorListItem {
  final bool? adult;
  final int? gender;
  final int? id;

  @JsonKey(name: 'known_for_department')
  final String? job;

  final String? originalName;
  final double? popularity;
  final String? profilePath;
  final int? castId;
  final String? character;
  final String? creditId;
  final int? order;

  factory ActorListItem.fromJson(Map<String, dynamic> json) =>
      _$ActorListItemFromJson(json);

  Map<String, dynamic> toJson() => _$ActorListItemToJson(this);

  const ActorListItem({
    this.adult,
    this.gender,
    this.id,
    this.job,
    this.originalName,
    this.popularity,
    this.profilePath,
    this.castId,
    this.character,
    this.creditId,
    this.order,
  });
}
