import 'package:flavoring/model/entity/movie/movie_list_item.dart';
import 'package:json_annotation/json_annotation.dart';

part 'collection_detail.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class CollectionDetail {
  final int? id;
  final String? name;
  final String? posterPath;
  final String? overview;
  final String? backdropPath;
  final List<MovieListItem>? parts;

  factory CollectionDetail.fromJson(Map<String, dynamic> json) =>
      _$CollectionDetailFromJson(json);

  Map<String, dynamic> toJson() => _$CollectionDetailToJson(this);

  const CollectionDetail({
    this.id,
    this.name,
    this.posterPath,
    this.overview,
    this.backdropPath,
    this.parts,
  });
}
