import 'package:flavoring/configuration/environment/env.dart';
import 'package:flavoring/presentation/app.dart';
import 'package:flavoring/core/injection.dart';

import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterFatalError;

  await setupInjection(AppEnv.dev);

  runApp(const MyApp());
}
