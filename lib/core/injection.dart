import 'package:flavoring/configuration/environment/env.dart';
import 'package:flavoring/core/db_helper/db_helper_barrel.dart';
import 'package:flavoring/data/data_source/remote/remote_barrel.dart';
import 'package:flavoring/data/repository/repository_impl_barrel.dart';
import 'package:flavoring/domain/repository/repository_barrel.dart';
import 'package:flavoring/domain/use_case/use_case_barrel.dart';

import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';

GetIt getIt = GetIt.I;

Future<void> setupInjection(AppEnv flavor) async {
  _registerAppConfig(flavor);
  _registerNetworkService();
  await _registerLocalStorage();
  _registerRepository();
  _registerUseCase();
}

void _registerNetworkService() {
  Dio dio = Dio(
      BaseOptions(baseUrl: getIt<AppConfig>().baseUrl, connectTimeout: 10000));

  dio.interceptors.addAll([
    AuthInterceptor(apiKey: getIt<AppConfig>().apiKey),
    PrettyDioLogger(requestBody: true),
  ]);

  getIt.registerFactory<Dio>(() => dio);

  getIt.registerFactory<AuthService>(
      () => AuthService(dio, baseUrl: dio.options.baseUrl));

  getIt.registerFactory<MovieService>(
      () => MovieService(dio, baseUrl: '${dio.options.baseUrl}/movie'));

  getIt.registerFactory<CollectionService>(() =>
      CollectionService(dio, baseUrl: '${dio.options.baseUrl}/collection'));
}

Future<void> _registerLocalStorage() async {
  await SharedPreferenceHelper.instance.init();
}

void _registerRepository() {
  getIt.registerFactory<AuthRepository>(() => AuthRepositoryImpl());

  getIt.registerFactory<MovieRepository>(() => MovieRepositoryImpl(getIt()));

  getIt.registerFactory<CollectionRepository>(
      () => CollectionRepositoryImpl(getIt()));
}

void _registerUseCase() {
  ///Movie
  getIt.registerFactory(() => GetPopularMovieUseCase(getIt()));
  getIt.registerFactory(() => GetLatestMovieUseCase(getIt()));
  getIt.registerFactory(() => GetUpcomingMovieUseCase(getIt()));
  getIt.registerFactory(() => GetTopRatedMovieUseCase(getIt()));
  getIt.registerFactory(() => GetMovieDetailUseCase(getIt()));
  getIt.registerFactory(() => GetSimilarMovieUseCase(getIt()));
  getIt.registerFactory(() => GetRecommendationMovieUseCase(getIt()));
  getIt.registerFactory(() => GetMovieReviewUseCase(getIt()));
  getIt.registerFactory(() => GetMovieCreditUseCase(getIt()));

  ///Collection
  getIt.registerFactory(() => GetCollectionDetailUseCase(getIt()));
}

void _registerAppConfig(AppEnv flavor) {
  switch (flavor) {
    case AppEnv.dev:
      getIt.registerSingleton<AppConfig>(AppConfigDev());
      break;
    case AppEnv.prod:
      getIt.registerSingleton<AppConfig>(AppConfigProduct());
      break;
  }

  // getIt.registerSingleton<AppTheme>(AppTheme(CustomTheme.light));
}
