import 'package:flavoring/presentation/actor/actor_route.dart';
import 'package:flavoring/presentation/auth/auth_route.dart';
import 'package:flavoring/presentation/home/home_route.dart';
import 'package:flavoring/presentation/main/main_page.dart';
import 'package:flavoring/presentation/movie/movie_route.dart';

import 'package:flavoring/presentation/splash/splash_route.dart';

import 'package:flutter/material.dart';

class RouteDefine {
  static String root = '/';

  static String main = '/main';

  static String home = 'home';

  ///Auth
  static String login = 'login';
  static String register = 'register';

  ///Movie
  static String movieList = '/movieList';
  static String movieDetail = '/movieDetail';

  ///Actor
  static String actor = '/actor';
}

class AppRoute {
  static MaterialPageRoute generateRoute(RouteSettings settings) {
    final routes = <String, WidgetBuilder>{
      RouteDefine.root: (_) => SplashRoute.route,

      ///Auth
      RouteDefine.login: (_) => AuthRoute.login,
      RouteDefine.register: (_) => AuthRoute.register,

      RouteDefine.main: (_) => const MainPage(),

      ///Home
      RouteDefine.home: (_) => HomeRoute.route,

      ///Movie
      RouteDefine.movieList: (_) => MovieRoute.list,
      RouteDefine.movieDetail: (_) => MovieRoute.detail,

      ///Actor
      RouteDefine.actor: (_) => ActorRoute.route
    };

    final routeBuilder = routes[settings.name];

    return MaterialPageRoute(
      builder: (context) => routeBuilder!(context),
      settings: settings,
    );
  }
}
