import 'package:flavoring/domain/repository/collection_repository.dart';
import 'package:flavoring/model/entity/collection/collection_detail.dart';
import 'package:flavoring/model/exception/api_exception.dart';

class GetCollectionDetailUseCase {
  final CollectionRepository repository;

  GetCollectionDetailUseCase(this.repository);

  Future<CollectionDetail> call(int collectionId) async {
    try {
      return await repository.getCollectionDetail(collectionId);
    } on ApiException catch (_) {
      rethrow;
    }
  }
}
