import 'package:flavoring/domain/repository/movie_repository.dart';
import 'package:flavoring/model/entity/movie/movie_detail.dart';

import 'package:flavoring/model/exception/api_exception.dart';

class GetLatestMovieUseCase {
  final MovieRepository repository;

  GetLatestMovieUseCase(this.repository);

  Future<MovieDetail> call({int? page}) async {
    try {
      return await repository.getLatestMovie();
    } on ApiException catch (_) {
      rethrow;
    }
  }
}
