import 'package:flavoring/domain/repository/movie_repository.dart';
import 'package:flavoring/model/entity/movie/movie_detail.dart';

import 'package:flavoring/model/exception/api_exception.dart';

class GetMovieDetailUseCase {
  final MovieRepository repository;

  GetMovieDetailUseCase(this.repository);

  Future<MovieDetail> call(int movieId) async {
    try {
      return await repository.getDetailMovie(movieId);
    } on ApiException catch (_) {
      rethrow;
    }
  }
}
