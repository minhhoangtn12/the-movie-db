import 'package:flavoring/domain/repository/movie_repository.dart';
import 'package:flavoring/model/entity/movie/movie_list_item.dart';
import 'package:flavoring/model/exception/api_exception.dart';
import 'package:flavoring/model/response/array_response.dart';

class GetPopularMovieUseCase {
  final MovieRepository repository;

  GetPopularMovieUseCase(this.repository);

  Future<ArrayResponse<MovieListItem>> call({int? page}) async {
    try {
      return await repository.getPopularMovie(page ?? 1);
    } on ApiException catch (_) {
      rethrow;
    }
  }
}
