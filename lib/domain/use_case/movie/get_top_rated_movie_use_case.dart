import 'package:flavoring/domain/repository/movie_repository.dart';
import 'package:flavoring/model/entity/movie/movie_list_item.dart';
import 'package:flavoring/model/exception/api_exception.dart';
import 'package:flavoring/model/response/array_response.dart';

class GetTopRatedMovieUseCase {
  final MovieRepository repository;

  GetTopRatedMovieUseCase(this.repository);

  Future<ArrayResponse<MovieListItem>> call({int? page}) async {
    try {
      return await repository.getTopRatedMovie(page ?? 1);
    } on ApiException catch (_) {
      rethrow;
    }
  }
}
