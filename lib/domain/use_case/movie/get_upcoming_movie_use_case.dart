import 'package:flavoring/domain/repository/repository_barrel.dart';
import 'package:flavoring/model/entity/movie/movie_list_item.dart';
import 'package:flavoring/model/exception/api_exception.dart';
import 'package:flavoring/model/response/array_response.dart';

class GetUpcomingMovieUseCase {
  final MovieRepository repository;

  GetUpcomingMovieUseCase(this.repository);

  Future<ArrayResponse<MovieListItem>> call({int? page}) async {
    try {
      return await repository.getUpcomingMovie(page ?? 1);
    } on ApiException catch (_) {
      rethrow;
    }
  }
}
