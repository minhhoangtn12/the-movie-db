import 'package:flavoring/domain/repository/movie_repository.dart';
import 'package:flavoring/model/entity/movie/movie_list_item.dart';

import 'package:flavoring/model/exception/api_exception.dart';
import 'package:flavoring/model/response/array_response.dart';

class GetRecommendationMovieUseCase {
  final MovieRepository repository;

  GetRecommendationMovieUseCase(this.repository);

  Future<ArrayResponse<MovieListItem>> call(
      {required int movieId, int? page}) async {
    try {
      return await repository.getRecommendationMovie(movieId, page ?? 1);
    } on ApiException catch (_) {
      rethrow;
    }
  }
}
