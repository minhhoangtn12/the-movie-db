import 'package:flavoring/domain/repository/movie_repository.dart';

import 'package:flavoring/model/entity/movie/movie_review.dart';

import 'package:flavoring/model/exception/api_exception.dart';
import 'package:flavoring/model/response/array_response.dart';

class GetMovieReviewUseCase {
  final MovieRepository repository;

  GetMovieReviewUseCase(this.repository);

  Future<ArrayResponse<MovieReview>> call(
      {required int movieId, int? page}) async {
    try {
      return await repository.getReviewMovie(movieId, page ?? 1);
    } on ApiException catch (_) {
      rethrow;
    }
  }
}
