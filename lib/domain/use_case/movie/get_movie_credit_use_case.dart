import 'package:flavoring/domain/repository/movie_repository.dart';

import 'package:flavoring/model/exception/api_exception.dart';
import 'package:flavoring/model/response/credit_response.dart';

class GetMovieCreditUseCase {
  final MovieRepository repository;

  GetMovieCreditUseCase(this.repository);

  Future<CreditResponse> call({required int movieId, int? limit}) async {
    try {
      final result = await repository.getCreditMovie(movieId);
      result.cast?.sort((a, b) => (a.order ?? 0).compareTo((b.order ?? 0)));
      result.crew?.sort((a, b) => (a.order ?? 0).compareTo((b.order ?? 0)));
      if (limit != null) {
        result.cast?.removeRange(limit, result.cast!.length);
        result.crew?.removeRange(limit, result.crew!.length);
      }
      return result;
    } on ApiException catch (_) {
      rethrow;
    }
  }
}
