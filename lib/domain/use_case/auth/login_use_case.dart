import 'package:flavoring/domain/repository/repository_barrel.dart';

class LoginUseCase {
  final AuthRepository authRepository;

  const LoginUseCase({
    required this.authRepository,
  });
}
