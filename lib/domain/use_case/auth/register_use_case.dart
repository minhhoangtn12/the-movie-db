import 'package:flavoring/domain/repository/repository_barrel.dart';

class RegisterUseCase {
  final AuthRepository authRepository;

  const RegisterUseCase({
    required this.authRepository,
  });
}
