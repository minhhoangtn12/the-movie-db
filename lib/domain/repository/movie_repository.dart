import 'package:flavoring/model/entity/movie/movie_detail.dart';
import 'package:flavoring/model/entity/movie/movie_list_item.dart';
import 'package:flavoring/model/entity/movie/movie_review.dart';
import 'package:flavoring/model/response/array_response.dart';
import 'package:flavoring/model/response/credit_response.dart';

abstract class MovieRepository {
  Future<ArrayResponse<MovieListItem>> getPopularMovie(int page);

  Future<ArrayResponse<MovieListItem>> getTopRatedMovie(int page);

  Future<ArrayResponse<MovieListItem>> getUpcomingMovie(int page);

  Future<MovieDetail> getLatestMovie();

  Future<MovieDetail> getDetailMovie(int id);

  Future<ArrayResponse<MovieListItem>> getSimilarMovie(int id, int page);

  Future<ArrayResponse<MovieListItem>> getRecommendationMovie(int id, int page);

  Future<ArrayResponse<MovieReview>> getReviewMovie(int id, int page);

  Future<CreditResponse> getCreditMovie(int id);
}
