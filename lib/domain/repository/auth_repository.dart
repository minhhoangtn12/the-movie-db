import 'package:flavoring/model/entity/user/user_entity.dart';
import 'package:flavoring/model/request/auth/login_request.dart';
import 'package:flavoring/model/request/auth/register_request.dart';

abstract class AuthRepository {
  UserEntity autoLogin(String token);
  Future<UserEntity> login(LoginRequest param);
  Future<void> registerAccount(RegisterRequest param);
  Future<void> logout();
}
