import 'package:flavoring/model/entity/collection/collection_detail.dart';

abstract class CollectionRepository {
  Future<CollectionDetail> getCollectionDetail(int id);
}
