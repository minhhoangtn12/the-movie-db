import 'package:flavoring/presentation/home/bloc/home_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'view/home_page.dart';

class HomeRoute {
  static Widget get route => BlocProvider(
        create: (context) => HomeBloc()..add(HomeRefresh()),
        child: const HomePage(),
      );
}
