part of 'upcoming_list_bloc.dart';

abstract class UpcomingListState extends Equatable {
  const UpcomingListState();
}

class UpcomingListLoading extends UpcomingListState {
  @override
  List<Object> get props => [];
}

class UpcomingListSuccess extends UpcomingListState {
  final List<MovieListItem> movies;

  @override
  List<Object?> get props => [movies];

  const UpcomingListSuccess({
    required this.movies,
  });
}

class UpcomingListFailure extends UpcomingListState {
  @override
  List<Object> get props => [];
}
