import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flavoring/domain/use_case/use_case_barrel.dart';
import 'package:flavoring/model/entity/movie/movie_list_item.dart';
import 'package:flavoring/model/exception/api_exception.dart';

part 'upcoming_list_event.dart';
part 'upcoming_list_state.dart';

class UpcomingListBloc extends Bloc<UpcomingListEvent, UpcomingListState> {
  final GetUpcomingMovieUseCase useCase;
  UpcomingListBloc(this.useCase) : super(UpcomingListLoading()) {
    on<UpcomingListFetched>((event, emit) async {
      emit(UpcomingListLoading());
      try {
        final result = await useCase.call();
        emit(UpcomingListSuccess(movies: result.results ?? []));
      } on ApiException catch (_) {
        emit(UpcomingListFailure());
      }
    });
  }
}
