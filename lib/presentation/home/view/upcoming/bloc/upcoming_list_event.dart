part of 'upcoming_list_bloc.dart';

abstract class UpcomingListEvent extends Equatable {
  const UpcomingListEvent();
}

class UpcomingListFetched extends UpcomingListEvent {
  @override
  List<Object?> get props => [];
}
