import 'package:flavoring/configuration/style/app_text_style.dart';
import 'package:flavoring/presentation/common/component/widget_movie_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/popular_list_bloc.dart';

class PopularListWidget extends StatelessWidget {
  const PopularListWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PopularListBloc, PopularListState>(
      builder: (context, state) {
        if (state is PopularListLoading) {
          return const CircularProgressIndicator();
        }

        if (state is PopularListSuccess) {
          return Container(
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(10)),
            child: Column(
              children: [
                const SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        'What\'s Popular',
                        style: AppTextStyle.black(18, weight: FontWeight.bold),
                      ),
                      Text(
                        'See all',
                        style: AppTextStyle.black(16, weight: FontWeight.w600),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 10),
                SizedBox(
                    height: 280,
                    child: ListView.separated(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          final item = state.movies[index];
                          return SizedBox(
                            width: 120,
                            child: WidgetMovieItem(
                                item: item,
                                titleStyle: AppTextStyle.black(14,
                                    weight: FontWeight.bold)),
                          );
                        },
                        separatorBuilder: (context, index) =>
                            const SizedBox(width: 10),
                        itemCount: state.movies.length)),
              ],
            ),
          );
        }
        return const SizedBox();
      },
    );
  }
}
