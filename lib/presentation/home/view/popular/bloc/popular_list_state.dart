part of 'popular_list_bloc.dart';

abstract class PopularListState extends Equatable {
  const PopularListState();
}

class PopularListLoading extends PopularListState {
  @override
  List<Object> get props => [];
}

class PopularListSuccess extends PopularListState {
  final List<MovieListItem> movies;

  const PopularListSuccess({required this.movies});

  @override
  List<Object?> get props => [movies];
}

class PopularListFailure extends PopularListState {
  @override
  List<Object> get props => [];
}
