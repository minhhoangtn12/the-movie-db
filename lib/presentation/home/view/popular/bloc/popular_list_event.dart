part of 'popular_list_bloc.dart';

abstract class PopularListEvent extends Equatable {
  const PopularListEvent();
}

class PopularListFetched extends PopularListEvent {
  @override
  List<Object?> get props => [];
}
