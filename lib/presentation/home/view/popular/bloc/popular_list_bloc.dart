import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flavoring/domain/use_case/movie/get_popular_movie_use_case.dart';
import 'package:flavoring/model/entity/movie/movie_list_item.dart';
import 'package:flavoring/model/exception/api_exception.dart';

part 'popular_list_event.dart';
part 'popular_list_state.dart';

class PopularListBloc extends Bloc<PopularListEvent, PopularListState> {
  final GetPopularMovieUseCase useCase;

  PopularListBloc(this.useCase) : super(PopularListLoading()) {
    on<PopularListFetched>((event, emit) async {
      emit(PopularListLoading());
      try {
        final result = await useCase.call();
        emit(PopularListSuccess(movies: result.results ?? []));
      } on ApiException catch (_) {
        emit(PopularListFailure());
      }
    });
  }
}
