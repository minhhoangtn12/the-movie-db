import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flavoring/domain/use_case/movie/get_latest_movie_use_case.dart';
import 'package:flavoring/model/entity/movie/movie_detail.dart';
import 'package:flavoring/model/exception/api_exception.dart';
import 'package:meta/meta.dart';

part 'latest_state.dart';
part 'latest_event.dart';

class LatestBloc extends Bloc<LatestEvent, LatestState> {
  final GetLatestMovieUseCase useCase;
  LatestBloc(this.useCase) : super(LatestLoading()) {
    on<LatestFetched>((event, emit) async {
      emit(LatestLoading());
      try {
        final result = await useCase.call();
        emit(LatestSuccess(result));
      } on ApiException catch (_) {
        emit(LatestFailure());
      }
    });
  }
}
