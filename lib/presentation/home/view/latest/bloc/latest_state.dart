part of 'latest_bloc.dart';

abstract class LatestState extends Equatable {
  const LatestState();
}

class LatestLoading extends LatestState {
  @override
  List<Object> get props => [];
}

class LatestSuccess extends LatestState {
  final MovieDetail movie;
  const LatestSuccess(this.movie);

  @override
  List<Object?> get props => [movie];
}

class LatestFailure extends LatestState {
  @override
  List<Object> get props => [];
}
