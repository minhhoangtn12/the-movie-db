part of 'latest_bloc.dart';

@immutable
abstract class LatestEvent extends Equatable {}

class LatestFetched extends LatestEvent {
  @override
  List<Object?> get props => [];
}
