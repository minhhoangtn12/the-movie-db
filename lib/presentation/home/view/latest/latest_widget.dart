import 'package:flavoring/configuration/style/app_colors.dart';
import 'package:flavoring/configuration/style/app_text_style.dart';
import 'package:flavoring/core/routing/app_router.dart';
import 'package:flavoring/presentation/common/widget_cached_image.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/latest_bloc.dart';

class LatestWidget extends StatelessWidget {
  const LatestWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LatestBloc, LatestState>(
      builder: (context, state) {
        if (state is LatestLoading) return const CircularProgressIndicator();
        if (state is LatestSuccess) {
          final movieInfo = state.movie;
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () =>
                    Navigator.of(context).pushNamed(RouteDefine.movieDetail),
                child: SizedBox(
                  height: 250,
                  width: double.infinity,
                  child: WidgetCachedImage(
                    url: movieInfo.backdropPath,
                  ),
                ),
              ),
              const SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.all(10),
                child: Text(
                  '${movieInfo.title}',
                  style: AppTextStyle.white(20, weight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  children: [
                    ..._buildStarRating(movieInfo.starVoteRate),
                    const SizedBox(width: 10),
                    Text(
                      '${movieInfo.voteCount} votes',
                      style: AppTextStyle.white(16),
                    )
                  ],
                ),
              ),
              const SizedBox(height: 10),
              GestureDetector(
                onTap: () => Navigator.of(context).pushNamed(RouteDefine.actor),
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, bottom: 10),
                  child: Wrap(
                    runSpacing: 10,
                    spacing: 10,
                    children: List.generate(
                      movieInfo.genres?.length ?? 0,
                      (index) => Container(
                        decoration: BoxDecoration(
                            color: index == 0 ? AppColor.greenMain : null,
                            borderRadius: BorderRadius.circular(5)),
                        padding: const EdgeInsets.symmetric(
                            vertical: 5, horizontal: 20),
                        child: Text(
                          '${movieInfo.genres![index].name}',
                          style: index == 0
                              ? AppTextStyle.black(14)
                              : AppTextStyle.white(14),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        }
        return const SizedBox();
      },
    );
  }

  _buildStarRating(double star) {
    int threshold = star ~/ 1;
    List<Icon> list = [];
    for (int i = 0; i <= threshold; i++) {
      if (i == threshold) {
        if ((star % 1) != 0) {
          list.add(Icon(
            Icons.star_half,
            size: 18,
            color: AppColor.greenMain,
          ));
        }
        break;
      }
      list.add(Icon(
        Icons.star_outlined,
        size: 18,
        color: AppColor.greenMain,
      ));
    }
    for (int i = list.length; i < 5; i++) {
      list.add(Icon(
        Icons.star_border_outlined,
        size: 18,
        color: AppColor.greenMain,
      ));
    }

    return List.generate(
      list.length,
      (index) => list[index],
    );
  }
}
