import 'package:flavoring/configuration/style/style_barrel.dart';
import 'package:flavoring/presentation/common/component/widget_movie_item.dart';
import 'package:flavoring/presentation/common/widget_cached_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/top_rated_bloc.dart';

class TopRatedWidget extends StatelessWidget {
  const TopRatedWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TopRatedBloc, TopRatedState>(
      builder: (context, state) {
        if (state is TopRatedLoading) {
          return const CircularProgressIndicator();
        }
        if (state is TopRatedSuccess) {
          return WidgetCachedBackgroundImage(
            url: state.movies.first.backdropPath,
            opacity: 0.4,
            child: Column(
              children: [
                const SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        'Top Rated',
                        style: AppTextStyle.white(18, weight: FontWeight.bold),
                      ),
                      Text(
                        'See all',
                        style: AppTextStyle.white(16, weight: FontWeight.w600),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 10),
                SizedBox(
                    height: 280,
                    child: ListView.separated(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          final item = state.movies[index];
                          return SizedBox(
                            width: 120,
                            child: WidgetMovieItem(
                              item: item,
                              titleStyle: AppTextStyle.white(14,
                                  weight: FontWeight.bold),
                            ),
                          );
                        },
                        separatorBuilder: (context, index) =>
                            const SizedBox(width: 10),
                        itemCount: state.movies.length)),
              ],
            ),
          );
        }
        return const SizedBox();
      },
    );
  }
}
