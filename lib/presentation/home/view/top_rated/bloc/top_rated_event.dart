part of 'top_rated_bloc.dart';

abstract class TopRatedEvent extends Equatable {
  const TopRatedEvent();
}

class TopRatedFetched extends TopRatedEvent {
  @override
  List<Object?> get props => [];
}
