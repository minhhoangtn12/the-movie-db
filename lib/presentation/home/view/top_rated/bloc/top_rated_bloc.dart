import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flavoring/domain/use_case/movie/get_top_rated_movie_use_case.dart';
import 'package:flavoring/model/entity/movie/movie_list_item.dart';
import 'package:flavoring/model/exception/api_exception.dart';

part 'top_rated_event.dart';
part 'top_rated_state.dart';

class TopRatedBloc extends Bloc<TopRatedEvent, TopRatedState> {
  GetTopRatedMovieUseCase useCase;
  TopRatedBloc(this.useCase) : super(TopRatedLoading()) {
    on<TopRatedFetched>((event, emit) async {
      emit(TopRatedLoading());
      try {
        final result = await useCase.call();

        emit(TopRatedSuccess(movies: result.results ?? []));
      } on ApiException catch (_) {
        emit(TopRatedFailure());
      }
    });
  }
}
