part of 'top_rated_bloc.dart';

abstract class TopRatedState extends Equatable {
  const TopRatedState();
}

class TopRatedLoading extends TopRatedState {
  @override
  List<Object> get props => [];
}

class TopRatedSuccess extends TopRatedState {
  final List<MovieListItem> movies;

  @override
  List<Object?> get props => [movies];

  const TopRatedSuccess({
    required this.movies,
  });
}

class TopRatedFailure extends TopRatedState {
  @override
  List<Object> get props => [];
}
