import 'package:flavoring/configuration/style/style_barrel.dart';
import 'package:flavoring/core/injection.dart';
import 'package:flavoring/presentation/home/bloc/home_bloc.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'latest/bloc/latest_bloc.dart';
import 'latest/latest_widget.dart';
import 'popular/bloc/popular_list_bloc.dart';
import 'popular/popular_list_widget.dart';
import 'top_rated/bloc/top_rated_bloc.dart';
import 'top_rated/top_rated_widget.dart';
import 'upcoming/bloc/upcoming_list_bloc.dart';
import 'upcoming/upcoming_list_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PopularListBloc popularListBloc = PopularListBloc(getIt());
  LatestBloc latestBloc = LatestBloc(getIt());
  UpcomingListBloc upcomingListBloc = UpcomingListBloc(getIt());
  TopRatedBloc topRatedBloc = TopRatedBloc(getIt());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<HomeBloc, HomeState>(
      listener: (context, state) {
        if (state is HomeLoading) {
          popularListBloc.add(PopularListFetched());
          latestBloc.add(LatestFetched());
          upcomingListBloc.add(UpcomingListFetched());
          topRatedBloc.add(TopRatedFetched());
        }
      },
      child: Scaffold(
        backgroundColor: AppColor.blueMain,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: AppColor.blueMain,
          automaticallyImplyLeading: false,
          title: const Text('Home'),
        ),
        body: BlocListener<HomeBloc, HomeState>(
          listener: (context, state) {},
          child: RefreshIndicator(
            onRefresh: () async {
              context.read<HomeBloc>().add(HomeRefresh());
            },
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: Column(
                children: [
                  BlocProvider.value(
                    value: latestBloc,
                    child: const LatestWidget(),
                  ),
                  BlocProvider.value(
                    value: popularListBloc,
                    child: const PopularListWidget(),
                  ),
                  BlocProvider.value(
                    value: upcomingListBloc,
                    child: const UpcomingListWidget(),
                  ),
                  BlocProvider.value(
                    value: topRatedBloc,
                    child: const TopRatedWidget(),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
