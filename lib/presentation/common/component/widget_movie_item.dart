import 'package:flavoring/configuration/style/style_barrel.dart';
import 'package:flavoring/core/routing/app_router.dart';
import 'package:flavoring/model/entity/movie/movie_list_item.dart';
import 'package:flavoring/presentation/common/widget_cached_image.dart';
import 'package:flutter/material.dart';

class WidgetMovieItem extends StatelessWidget {
  final MovieListItem item;
  final TextStyle titleStyle;
  const WidgetMovieItem(
      {Key? key, required this.item, required this.titleStyle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context)
          .pushNamed(RouteDefine.movieDetail, arguments: item.id!),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            clipBehavior: Clip.none,
            children: [
              Container(
                clipBehavior: Clip.hardEdge,
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(12)),
                height: 180,
                child: WidgetCachedImage(url: item.posterPath),
              ),
              Positioned(bottom: -18, left: 10, child: _buildRating())
            ],
          ),
          const SizedBox(height: 25),
          Text(
            '${item.title}',
            maxLines: 3,
            style: titleStyle,
            overflow: TextOverflow.ellipsis,
          ),
          const SizedBox(height: 5),
          Text(
            '${item.releaseDate}',
            style: AppTextStyle.grey(12),
          )
        ],
      ),
    );
  }

  _buildRating() {
    late Color indicatorColor;
    final ratePercent = (item.voteAverage ?? 0) * 10;

    if (ratePercent < 50) {
      indicatorColor = Colors.red;
    } else if (ratePercent < 75) {
      indicatorColor = Colors.yellow;
    } else {
      indicatorColor = Colors.green;
    }
    return Stack(
      alignment: Alignment.center,
      children: [
        SizedBox(
          height: 36,
          width: 36,
          child: CircularProgressIndicator(
            backgroundColor: Colors.grey,
            color: indicatorColor,
            value: ratePercent / 100,
          ),
        ),
        CircleAvatar(
          radius: 16,
          backgroundColor: Colors.black87,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                item.ratePercentText,
                style: AppTextStyle.white(12),
              ),
              Text(
                '%',
                style: AppTextStyle.white(5),
              )
            ],
          ),
        )
      ],
    );
  }
}
