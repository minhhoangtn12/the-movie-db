import 'package:cached_network_image/cached_network_image.dart';
import 'package:flavoring/configuration/environment/env.dart';
import 'package:flavoring/configuration/style/style_barrel.dart';
import 'package:flavoring/core/injection.dart';
import 'package:flutter/material.dart';

class WidgetCachedImage extends StatelessWidget {
  final String? url;
  const WidgetCachedImage({Key? key, this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final config = getIt<AppConfig>();
    return CachedNetworkImage(
      fit: BoxFit.fill,
      httpHeaders: {
        'api_key': config.apiKey,
      },
      imageUrl: '${config.imageUrl}/original$url',
      errorWidget: (context, _, __) => Image.asset(
        AppImage.icLogo,
        fit: BoxFit.cover,
      ),
      progressIndicatorBuilder: (context, _, process) {
        return Center(
          child: SizedBox(
            height: 30,
            width: 30,
            child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(AppColor.blueMain),
                value: process.progress),
          ),
        );
      },
    );
  }
}

class WidgetCachedBackgroundImage extends StatelessWidget {
  final String? url;
  final Widget child;
  final ColorFilter? colorFilter;
  final double opacity;
  const WidgetCachedBackgroundImage(
      {Key? key,
      this.url,
      required this.child,
      this.opacity = 1.0,
      this.colorFilter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final config = getIt<AppConfig>();
    return CachedNetworkImage(
      fit: BoxFit.fill,
      httpHeaders: {
        'api_key': config.apiKey,
      },
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          image: imageProvider,
          colorFilter: colorFilter,
          fit: BoxFit.fill,
          opacity: opacity,
        )),
        child: child,
      ),
      imageUrl: '${config.imageUrl}/original$url',
      errorWidget: (context, _, __) => Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: const AssetImage(AppImage.icLogo),
                colorFilter: colorFilter,
                fit: BoxFit.fill,
                opacity: opacity)),
        child: child,
      ),
    );
  }
}
