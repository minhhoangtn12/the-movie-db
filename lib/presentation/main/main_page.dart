import 'package:flavoring/configuration/style/app_colors.dart';
import 'package:flavoring/presentation/home/home_route.dart';
import 'package:flavoring/presentation/home/view/home_page.dart';

import 'package:flavoring/presentation/movie/movie_route.dart';
import 'package:flutter/material.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
              child: PageView(
            children: [
              HomeRoute.route,
              // Navigator(
              //   initialRoute: RouteDefine.home,
              //   onGenerateRoute: AppRoute.generateRoute,
              // ),
              MovieRoute.list,
              HomePage(),
              HomePage(),
            ],
          )),
          BottomNavigationBar(
            items: [
              BottomNavigationBarItem(
                  backgroundColor: AppColor.blueMain,
                  icon: Icon(Icons.home),
                  label: 'Home'),
              BottomNavigationBarItem(icon: Icon(Icons.movie), label: 'Movies'),
              BottomNavigationBarItem(icon: Icon(Icons.tv), label: 'TV Shows'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.settings), label: 'Setting'),
            ],
          )
        ],
      ),
    );
  }
}
