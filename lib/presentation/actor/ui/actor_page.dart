import 'package:flavoring/configuration/style/app_text_style.dart';
import 'package:flutter/material.dart';

class ActorPage extends StatelessWidget {
  const ActorPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dwayne Johnson'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 20),
            CircleAvatar(
              backgroundColor: Colors.tealAccent,
              radius: 80,
            ),
            SizedBox(height: 30),
            Text(
              'Dwayne Johnson',
              style: AppTextStyle.black(30),
            ),
            Text('acting'),
            SizedBox(height: 20),
            Text(
                'after the incident in hawair, he workasd asdh lkhsdkj kjqwheqjhw kjashd, hasbd, jashdk'),
            SizedBox(height: 30),
            Text('PHOTO'),
            SizedBox(
                height: 150,
                child: ListView.separated(
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) => Container(
                          width: 100,
                          color: Colors.red,
                        ),
                    separatorBuilder: (context, index) => SizedBox(width: 10),
                    itemCount: 5)),
            Text('Knownfor'),
            SizedBox(
                height: 200,
                child: ListView.separated(
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) => SizedBox(
                          width: 100,
                          child: Column(
                            children: [
                              Container(
                                height: 180,
                                color: Colors.green,
                              ),
                              Text(
                                'Money plane',
                                overflow: TextOverflow.ellipsis,
                              )
                            ],
                          ),
                        ),
                    separatorBuilder: (context, index) => SizedBox(width: 10),
                    itemCount: 5)),
            SizedBox(height: 20),
            Text('Acting'),
            ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (context, index) => Row(
                children: [
                  Text('2024'),
                  SizedBox(width: 10),
                  CircleAvatar(
                    radius: 5,
                    backgroundColor: Colors.black87,
                  ),
                  Text('Despicable Me 4 '),
                  Text('as Minions(voice')
                ],
              ),
              itemCount: 5,
            ),
          ],
        ),
      ),
    );
  }
}
