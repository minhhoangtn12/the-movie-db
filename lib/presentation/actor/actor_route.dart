import 'package:flavoring/presentation/actor/ui/actor_page.dart';
import 'package:flutter/material.dart';

class ActorRoute {
  static Widget get route => ActorPage();
}
