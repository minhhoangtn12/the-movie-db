import 'package:flavoring/core/injection.dart';
import 'package:flavoring/presentation/movie/movie_detail/view/movie_detail_page.dart';
import 'package:flavoring/presentation/movie/movie_list/view/movie_list_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'movie_detail/bloc/movie_detail_cubit.dart';

class MovieRoute {
  static Widget get list => const MovieListPage();

  static Widget get detail => Builder(builder: (context) {
        final param = ModalRoute.of(context)!.settings.arguments as int;
        return BlocProvider(
          lazy: false,
          create: (context) => MovieDetailCubit(getIt())..getMovieDetail(param),
          child: MovieDetailPage(),
        );
      });
}
