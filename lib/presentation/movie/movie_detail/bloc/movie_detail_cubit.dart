import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flavoring/domain/use_case/movie/get_movie_detail_use_case.dart';
import 'package:flavoring/model/entity/movie/movie_detail.dart';
import 'package:flavoring/model/exception/api_exception.dart';

part 'movie_detail_state.dart';

class MovieDetailCubit extends Cubit<MovieDetailState> {
  GetMovieDetailUseCase useCase;
  MovieDetailCubit(this.useCase) : super(MovieDetailInitial());

  void getMovieDetail(int id) async {
    emit(MovieDetailLoading());

    try {
      final result = await useCase.call(id);
      emit(MovieDetailSuccess(detail: result));
    } on ApiException catch (_) {
      emit(MovieDetailFailure());
    }
  }
}
