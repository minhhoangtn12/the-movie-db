part of 'movie_detail_cubit.dart';

abstract class MovieDetailState extends Equatable {
  const MovieDetailState();
}

class MovieDetailInitial extends MovieDetailState {
  @override
  List<Object> get props => [];
}

class MovieDetailLoading extends MovieDetailState {
  @override
  List<Object> get props => [];
}

class MovieDetailSuccess extends MovieDetailState {
  final MovieDetail detail;

  String get genresText {
    String text = '';
    detail.genres?.forEach((element) {
      text += '${element.name}, ';
    });
    text = text.substring(0, text.length - 2);
    return text;
  }

  @override
  List<Object> get props => [detail];

  const MovieDetailSuccess({
    required this.detail,
  });
}

class MovieDetailFailure extends MovieDetailState {
  @override
  List<Object> get props => [];
}
