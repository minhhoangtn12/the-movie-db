import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flavoring/domain/use_case/use_case_barrel.dart';
import 'package:flavoring/model/entity/actor/actor_list_item.dart';
import 'package:flavoring/model/exception/api_exception.dart';

part 'cast_state.dart';

class CastCubit extends Cubit<CastState> {
  GetMovieCreditUseCase useCase;
  CastCubit(this.useCase) : super(CastInitial());

  void getMovieCredit(int movieId) async {
    emit(CastLoading());
    try {
      final result = await useCase.call(movieId: movieId, limit: 15);
      emit(CastSuccess(casts: result.cast ?? []));
    } on ApiException catch (_) {
      emit(CastFailure());
    }
  }
}
