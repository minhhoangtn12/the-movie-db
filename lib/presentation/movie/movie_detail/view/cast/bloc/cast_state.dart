part of 'cast_cubit.dart';

abstract class CastState extends Equatable {
  const CastState();
}

class CastInitial extends CastState {
  @override
  List<Object> get props => [];
}

class CastLoading extends CastState {
  @override
  List<Object> get props => [];
}

class CastSuccess extends CastState {
  final List<ActorListItem> casts;

  @override
  List<Object> get props => [casts];

  const CastSuccess({
    required this.casts,
  });
}

class CastFailure extends CastState {
  @override
  List<Object> get props => [];
}
