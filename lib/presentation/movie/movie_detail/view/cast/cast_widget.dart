import 'package:flavoring/presentation/common/widget_cached_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/cast_cubit.dart';

class CastWidget extends StatelessWidget {
  const CastWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CastCubit, CastState>(
      builder: (context, state) {
        if (state is CastLoading) return CircularProgressIndicator();
        if (state is CastSuccess) {
          return Column(
            children: [
              SizedBox(height: 10),
              Text('CAST'),
              SizedBox(
                  height: 150,
                  child: ListView.separated(
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        final cast = state.casts[index];
                        return SizedBox(
                          width: 100,
                          child: WidgetCachedImage(
                            url: cast.profilePath,
                          ),
                        );
                      },
                      separatorBuilder: (context, index) => SizedBox(width: 10),
                      itemCount: state.casts.length)),
            ],
          );
        }
        return SizedBox();
      },
    );
  }
}
