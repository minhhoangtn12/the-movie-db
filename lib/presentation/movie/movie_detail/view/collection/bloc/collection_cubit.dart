import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flavoring/domain/use_case/use_case_barrel.dart';
import 'package:flavoring/model/entity/movie/movie_list_item.dart';
import 'package:flavoring/model/exception/api_exception.dart';

part 'collection_state.dart';

class CollectionCubit extends Cubit<CollectionState> {
  GetCollectionDetailUseCase useCase;
  CollectionCubit(this.useCase) : super(CollectionInitial());

  void getCollectionDetail(int? id) async {
    if (id == null) {
      return;
    }
    emit(CollectionLoading());
    try {
      final result = await useCase.call(id);
      emit(CollectionSuccess(movies: result.parts ?? []));
    } on ApiException catch (_) {
      emit(CollectionFailure());
    }
  }
}
