part of 'collection_cubit.dart';

abstract class CollectionState extends Equatable {
  const CollectionState();
}

class CollectionInitial extends CollectionState {
  @override
  List<Object> get props => [];
}

class CollectionLoading extends CollectionState {
  @override
  List<Object> get props => [];
}

class CollectionSuccess extends CollectionState {
  final List<MovieListItem> movies;
  @override
  List<Object> get props => [movies];

  const CollectionSuccess({
    required this.movies,
  });
}

class CollectionFailure extends CollectionState {
  @override
  List<Object> get props => [];
}
