import 'package:flavoring/configuration/style/style_barrel.dart';
import 'package:flavoring/model/entity/collection/collection_detail.dart';
import 'package:flavoring/presentation/common/common_barrel.dart';
import 'package:flavoring/presentation/common/widget_cached_image.dart';
import 'package:flutter/material.dart';

class CollectionWidget extends StatelessWidget {
  final CollectionDetail? collectionInfo;
  const CollectionWidget({Key? key, required this.collectionInfo})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final info = collectionInfo;
    if (info == null) return const SizedBox();
    return SizedBox(
      height: 200,
      width: double.infinity,
      child: WidgetCachedBackgroundImage(
        url: info.backdropPath,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: AppColor.blueMain,
              child: Text(
                'Part of the ${info.name}',
                style: AppTextStyle.white(20),
              ),
            ),
            const SizedBox(height: 10),
            WidgetButton(
              width: 200,
              color: AppColor.blueMain,
              title: 'VIEW THE COLLECTION',
            ),
          ],
        ),
      ),
    );
  }
}
