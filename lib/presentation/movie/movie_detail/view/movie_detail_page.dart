import 'package:flavoring/configuration/style/app_text_style.dart';
import 'package:flavoring/core/injection.dart';
import 'package:flavoring/presentation/common/widget_cached_image.dart';
import 'package:flavoring/presentation/movie/movie_detail/bloc/movie_detail_cubit.dart';
import 'package:flavoring/presentation/movie/movie_detail/view/cast/bloc/cast_cubit.dart';
import 'package:flavoring/presentation/movie/movie_detail/view/cast/cast_widget.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'collection/collection_widget.dart';
import 'recommendation/bloc/recommendation_cubit.dart';
import 'recommendation/recommendation_widget.dart';
import 'review/bloc/review_cubit.dart';
import 'review/review_widget.dart';

class MovieDetailPage extends StatelessWidget {
  const MovieDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    RecommendationCubit recommendationCubit = RecommendationCubit(getIt());
    ReviewCubit reviewCubit = ReviewCubit(getIt());
    CastCubit castCubit = CastCubit(getIt());
    return Scaffold(
      appBar: AppBar(
        title: Text('Project power'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            BlocConsumer<MovieDetailCubit, MovieDetailState>(
              listener: (context, state) {
                if (state is MovieDetailSuccess) {
                  recommendationCubit.getRecommendationMovie(state.detail.id!);
                  reviewCubit.getMovieReview(id: state.detail.id!);
                  castCubit.getMovieCredit(state.detail.id!);
                }
              },
              builder: (context, state) {
                if (state is MovieDetailLoading)
                  return CircularProgressIndicator();
                if (state is MovieDetailSuccess) {
                  return Column(
                    children: [
                      SizedBox(
                          height: 250,
                          width: double.infinity,
                          child: WidgetCachedImage(
                              url: state.detail.backdropPath)),
                      const SizedBox(height: 5),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '${state.detail.title}',
                                style: AppTextStyle.black(30),
                              ),
                              Text(state.genresText),
                              Row(
                                children: [
                                  ...List.generate(
                                    5,
                                    (index) => Icon(
                                      Icons.star_outlined,
                                      size: 17,
                                    ),
                                  ),
                                  SizedBox(width: 10),
                                  Text('${state.detail.voteCount} votes')
                                ],
                              ),
                            ],
                          )),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Icon(Icons.calendar_month),
                                  Text('${state.detail.releaseDate}')
                                ],
                              ),
                              Row(
                                children: [
                                  Icon(Icons.schedule),
                                  Text('${state.detail.runtime} mins')
                                ],
                              ),
                              Row(
                                children: [
                                  Icon(Icons.language),
                                  Text('${state.detail.originalLanguage}')
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: 20),
                      Text('${state.detail.overview}'),
                      SizedBox(height: 10),
                      Text('VIDEOS'),
                      SizedBox(
                          height: 100,
                          child: ListView.separated(
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index) => Container(
                                    color: Colors.green,
                                    width: 200,
                                  ),
                              separatorBuilder: (context, index) =>
                                  SizedBox(width: 10),
                              itemCount: 5)),
                      BlocProvider.value(
                        value: castCubit,
                        child: CastWidget(),
                      ),
                      BlocProvider.value(
                        value: reviewCubit,
                        child: ReviewWidget(),
                      ),
                      CollectionWidget(collectionInfo: state.detail.collection),
                      BlocProvider.value(
                        value: recommendationCubit,
                        child: RecommendationWidget(),
                      ),
                    ],
                  );
                }
                return SizedBox();
              },
            ),
          ],
        ),
      ),
    );
  }
}
