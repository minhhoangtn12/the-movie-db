import 'package:flavoring/configuration/style/app_text_style.dart';
import 'package:flavoring/presentation/common/component/widget_movie_item.dart';
import 'package:flavoring/presentation/common/widget_cached_image.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/recommendation_cubit.dart';

class RecommendationWidget extends StatelessWidget {
  const RecommendationWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecommendationCubit, RecommendationState>(
      builder: (context, state) {
        if (state is RecommendationLoading) return CircularProgressIndicator();
        if (state is RecommendationSuccess) {
          return WidgetCachedBackgroundImage(
            url: state.movies.first.backdropPath,
            opacity: 0.4,
            child: Column(
              children: [
                Text('Recommendation'),
                SizedBox(height: 10),
                SizedBox(
                    height: 280,
                    child: ListView.separated(
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) => SizedBox(
                              width: 120,
                              child: WidgetMovieItem(
                                  item: state.movies[index],
                                  titleStyle: AppTextStyle.black(14,
                                      weight: FontWeight.bold)),
                            ),
                        separatorBuilder: (context, index) =>
                            SizedBox(width: 10),
                        itemCount: state.movies.length)),
                SizedBox(height: 10),
              ],
            ),
          );
        }
        return SizedBox();
      },
    );
  }
}
