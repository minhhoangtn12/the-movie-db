import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flavoring/domain/use_case/use_case_barrel.dart';

import 'package:flavoring/model/entity/movie/movie_list_item.dart';
import 'package:flavoring/model/exception/api_exception.dart';

part 'recommendation_state.dart';

class RecommendationCubit extends Cubit<RecommendationState> {
  GetRecommendationMovieUseCase useCase;
  RecommendationCubit(this.useCase) : super(RecommendationInitial());

  void getRecommendationMovie(int movieId) async {
    emit(RecommendationLoading());
    try {
      final result = await useCase.call(movieId: movieId);
      emit(RecommendationSuccess(movies: result.results ?? []));
    } on ApiException catch (_) {
      emit(RecommendationFailure());
    }
  }
}
