part of 'recommendation_cubit.dart';

abstract class RecommendationState extends Equatable {
  const RecommendationState();
}

class RecommendationInitial extends RecommendationState {
  @override
  List<Object> get props => [];
}

class RecommendationLoading extends RecommendationState {
  @override
  List<Object> get props => [];
}

class RecommendationSuccess extends RecommendationState {
  final List<MovieListItem> movies;

  @override
  List<Object> get props => [movies];

  const RecommendationSuccess({
    required this.movies,
  });
}

class RecommendationFailure extends RecommendationState {
  @override
  List<Object> get props => [];
}
