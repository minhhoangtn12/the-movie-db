import 'package:flavoring/presentation/common/widget_cached_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/review_cubit.dart';

class ReviewWidget extends StatelessWidget {
  const ReviewWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReviewCubit, ReviewState>(
      builder: (context, state) {
        if (state is ReviewLoading) return CircularProgressIndicator();
        if (state is ReviewSuccess) {
          return Column(
            children: [
              SizedBox(height: 10),
              Text('REVIEW'),
              ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  final review = state.reviews[index];
                  return Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 60,
                        width: 60,
                        clipBehavior: Clip.hardEdge,
                        decoration: const BoxDecoration(shape: BoxShape.circle),
                        child: WidgetCachedImage(
                          url: review.authorDetails?.avatarPath,
                        ),
                      ),
                      SizedBox(width: 20),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('A Review by ${review.author?.toLowerCase()}'),
                            Text(
                                'Written by ${review.author} on ${review.createdAt}'),
                            SizedBox(height: 20),
                            Text('${review.content}')
                          ],
                        ),
                      )
                    ],
                  );
                },
                itemCount: state.reviews.length,
              ),
              SizedBox(
                height: 10,
              ),
            ],
          );
        }
        return SizedBox();
      },
    );
  }
}
