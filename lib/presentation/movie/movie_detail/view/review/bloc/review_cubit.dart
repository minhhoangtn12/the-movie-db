import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flavoring/domain/use_case/use_case_barrel.dart';
import 'package:flavoring/model/entity/movie/movie_review.dart';
import 'package:flavoring/model/exception/api_exception.dart';

part 'review_state.dart';

class ReviewCubit extends Cubit<ReviewState> {
  GetMovieReviewUseCase useCase;
  ReviewCubit(this.useCase) : super(ReviewInitial());

  void getMovieReview({required int id, int? page}) async {
    emit(ReviewLoading());
    try {
      final result = await useCase.call(movieId: id, page: page);
      emit(ReviewSuccess(reviews: result.results ?? []));
    } on ApiException catch (_) {
      emit(ReviewFailure());
    }
  }
}
