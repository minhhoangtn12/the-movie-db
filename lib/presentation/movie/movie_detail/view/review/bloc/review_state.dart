part of 'review_cubit.dart';

abstract class ReviewState extends Equatable {
  const ReviewState();
}

class ReviewInitial extends ReviewState {
  @override
  List<Object> get props => [];
}

class ReviewLoading extends ReviewState {
  @override
  List<Object> get props => [];
}

class ReviewSuccess extends ReviewState {
  final List<MovieReview> reviews;

  @override
  List<Object> get props => [reviews];

  const ReviewSuccess({
    required this.reviews,
  });
}

class ReviewFailure extends ReviewState {
  @override
  List<Object> get props => [];
}
